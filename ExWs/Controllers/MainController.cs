using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using ExDA;
using ExWs.DTO;

namespace ExWs.Controllers
{
	[Authorize]
	[ApiController]
	[Route("api/[controller]/[action]")]
	public class MainController : ControllerBase
	{
		private readonly ILogger<MainController> _logger;
		private readonly IDA _ctx;
		private string ClientCode
		{
			get
			{
				return HttpContext.User?.Identity?.Name ?? "";
			}
		}

		public MainController(ILogger<MainController> logger, IDA ctx)
		{
			_logger = logger;
			_ctx = ctx;
		}

		[HttpGet]
		public Result Ver()
		{
			return new Result() { message = "1.0.1", status = "OK" };
		}

		[HttpPost]
		[AllowAnonymous]
		public UserProfile Login(NamePwd cred)
		{
			var client = _ctx.ClientLogin(cred.name, cred.password);
			var p = new UserProfile();
			if (client == null)
			{
				return p;
			}
			var claims = new List<Claim>
			{
				new Claim("user", client.Code),
				new Claim("role", "Client"),
			};
			HttpContext.SignInAsync(new ClaimsPrincipal(new ClaimsIdentity(claims, "Cookies", "user", "role")));
			if (client.EntityType == DBConst.CL_TYPE_PERSON)
			{
				p.Name = client.FirstName + " " + client.LastName;
			}
			else
			{
				p.Name = client.CompanyName;
			}
			p.IsAuth = true;
			p.EntityType = client.EntityType;
			p.Code = client.Code;
			return p;
		}

		[HttpGet]
		[AllowAnonymous]
		public void Logout()
		{
			HttpContext.SignOutAsync();
		}

		[HttpPost]
		public Result ChangePassword(NamePwd cred)
		{
			return new Result() { status = "OK" };
		}

		[HttpGet]
		public AddressHeader GetAddresses()
		{
			AddressHeader ret = new AddressHeader();
			string code = ClientCode;
			var addresses = _ctx.GetClientAddresses(code);
			foreach (var adr in addresses)
			{
				ret.addressList.Add(new Address()
				{
					addressType = adr.AddressType,
					line = adr.Line,
					postalCode = adr.PostalCode,
					country = adr.Country

				});
				ret.Countries.Add(new Pair() { Key = "CA", Val = "Canada" });
				ret.Countries.Add(new Pair() { Key = "US", Val = "USA" });
				ret.AddressTypes.Add(new Pair() { Key = "R", Val = "Residential" });
				ret.AddressTypes.Add(new Pair() { Key = "M", Val = "Mailing" });
				ret.AddressTypes.Add(new Pair() { Key = "B", Val = "Business" });
			}
			return ret;
		}

		[HttpPost]
		public Result SaveAddress([FromBody] AddressHeader adr)
		{
			var cc = ClientCode;
			return new Result() {  status = "OK" };
		}
	}

	public class NamePwd
	{
		public string? name { get; set; }
		public string password { get; set; }
	}

	public class Result
	{
		public string status { get; set; }
		public string? message { get; set; }
	}
}
