using ExDA;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Rewrite;
using System.Net;
using System.Text.RegularExpressions;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers().AddJsonOptions(opt =>
{
    //opt.JsonSerializerOptions.ReferenceHandler = System.Text.Json.Serialization.ReferenceHandler.Preserve;
    //opt.JsonSerializerOptions.PropertyNamingPolicy = null;
});
//builder.Services.AddSqlServer<vbankContext>(builder.Configuration.GetConnectionString("DefaultConnection"));
//builder.Services.AddDbContext<vbankContext>(options => options.UseSqlServer("name=ConnectionStrings:DefaultConnection"));
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddSingleton(typeof(IDA), new DA(builder.Configuration.GetConnectionString("DefaultConnection")));
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors();
builder.Services.AddRouting();
builder.Services.AddAuthentication(opt => {
    opt.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    opt.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
}).AddCookie(opt => {
//    opt.LoginPath = "/";
    opt.Events.OnRedirectToLogin = ctx =>
   {
       ctx.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
       return Task.FromResult(0);
   };
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


//app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.Use((context, next) => 
{
    string v = context.Request.Path.Value;

	if (!v.StartsWith("/api") && ! v.Contains('.'))
    {
        context.Request.Path = "/index.html";
    }
    return next(context);
});

app.MapControllers();
app.MapDefaultControllerRoute();

//RewriteOptions rw = new RewriteOptions().AddIISUrlRewrite(new StringReader(File.ReadAllText("Web.config")));
//app.UseRewriter(rw);

app.UseDefaultFiles();
app.UseStaticFiles();
app.UseCors();

app.Run();
