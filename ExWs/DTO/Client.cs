﻿namespace ExWs.DTO
{
	public class Client
	{
		public string entityType { get; set; }
		public string? firstName { get; set; }
		public string? lastName { get; set; }
		public string? companyName { get; set; }

	}

	public class UserProfile
	{
		public bool IsAuth { get; set; } = false;
		public string Name { get; set; } = "";
		public string EntityType { get; set; } = "";
		public string Code { get; set; } = "";
	}

	public class AddressHeader
	{
		public string status { get; set; } = "";
		public List<Pair> Countries { get; set; } = new List<Pair>();
		public List<Pair> AddressTypes { get; set; } = new List<Pair>();
		public List<Address> addressList { get; set; } = new List<Address>();
	}

	public class Address
	{
		public string? line { get; set; } = "";
		public string? postalCode { get; set; } = "";
		public string? addressType { get; set; } = "";
		public string? country { get; set; } = "";
		public bool? delete { get; set; } = false;
		public bool? isNew { get; set; } = false;
	}

	public class Pair
	{
		public string Key { get; set; } = "";
		public string Val { get; set; } = "";

	}
}
