import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpRequest, HttpResponse } from '@angular/common/http'
import { catchError, delay, Observable, of, map, Subject } from 'rxjs';
import { AddressHeader, Result, UserProfile } from './models';

@Injectable({
  providedIn: 'root'
})
export class DataSrvService {
  public userProfile: UserProfile;
  public IsLoggedIn : boolean = false;
  public EvtLoggedChanged = new Subject<boolean>();

  constructor(private http: HttpClient ) { 
    this.userProfile = new UserProfile();
  }

  public logout():  Observable<any> {
    return this.http.get<Observable<any>>('http://localhost:63754/api/main/logout' ).pipe(
      map( () => { this.EvtLoggedChanged.next(false); }
      
    ));
  }

  public auth(usr: string, pwd: string): Observable<UserProfile> {
    return this.http.post<UserProfile>('http://localhost:63754/api/main/login', {name: usr, password: pwd}).
      pipe(map ( (userProfile) => {
        this.EvtLoggedChanged.next(userProfile.isAuth);
        return (userProfile);
      }));
  }

  public getAddresses(): Observable<AddressHeader> {
    return this.http.get<AddressHeader>('http://localhost:63754/api/main/getaddresses', {});
  }

  public getVer(): Observable<Result> {
    return this.http.get<Result>('http://localhost:63754/api/main/ver', {});
  }

  public changePassword(txt: string): Observable<Result> {
    return this.http.post<Result>('http://localhost:63754/api/main/changepassword', {password: txt});
  }

  public saveAddress(adr: AddressHeader): Observable<Result> {
    return this.http.post<Result>('http://localhost:63754/api/main/saveaddress', {status: adr.status, addressList: adr.addressList});
  }

  public SetLoggedIn(userProfile: UserProfile){
     localStorage.setItem('userId', userProfile.name);
  }

  public IsSignedIn(): Observable<boolean> {
    return this.getVer().pipe(
        map((result) => {
            let yes = result.status == 'OK';
            this.EvtLoggedChanged.next(yes);
            return yes;
        }),
        catchError((error) => {
            return of(false);
        }));
}
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      alert(error.message);
  
      // TODO: better job of transforming error for user consumption
      //this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
