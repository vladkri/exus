export class UserProfile{
    public isAuth : boolean = false;
    public name : string = '';
    public entityType : string = '';
    public code : string = '';
}

export class Result {
    public status: string = ''
    public message : string = '';
}

export  class AddressHeader {
    public status : string = '';
    public addressList : Address[] = [];
    public countries : Pair[] = [];
    public addressTypes : Pair[] = [];
}

export  class Address {
    public line : string = '';
    public postalCode : string = '';
    public addressType : string = '';
    public country : string = '';
    public delete : boolean = false;
    public isNew : boolean = false;
}

export  class Pair {
    public key : string = '';
    public val : string = '';
}
