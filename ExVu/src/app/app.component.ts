import { Component, Injectable, OnInit } from '@angular/core';
import { DataSrvService } from './data-srv.service';
import { UserProfile } from './models';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ThisReceiver } from '@angular/compiler';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ExVu';
  public usr: UserProfile = new UserProfile();
  public searchText: string;
  public searchRes: string = '';
  public IsLoggedIn : boolean = false

  constructor(private srv: DataSrvService, private rt : Router) {
    this.searchText = '';
    this.srv.EvtLoggedChanged.subscribe(
      logged => {
        this.IsLoggedIn = logged;
//        this.srv.EvtLoggedChanged.next(logged);
      }
    )
  }

  ngOnInit(): void {
    this.srv.IsSignedIn().subscribe(s => {
       this.IsLoggedIn = s;
    });
  }

  public Logout() {
    this.srv.logout().subscribe();
    this.rt.navigate(['login']);
//    this.LoggedIn = false;
  }
}
