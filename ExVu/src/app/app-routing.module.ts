import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChangePasswordComponent } from './pages/settings/changePassword/change-password.component';
import { AddressComponent  } from './pages/settings/address/address.component';
import { AuthGGuard } from './auth-g.guard';
import { LoginComponent } from './pages/login.component';

const routes: Routes = [
  { path: 'changePassword', component: ChangePasswordComponent, canActivate: [AuthGGuard] },
  { path: 'address', component: AddressComponent, canActivate: [AuthGGuard]},
  { path: 'login', component: LoginComponent},
  { path: '', redirectTo: '/', pathMatch: 'full'},
  { path: '**', redirectTo: '/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
