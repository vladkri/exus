import { Component } from '@angular/core';
import { DataSrvService } from '.././data-srv.service';
import { UserProfile } from '.././models';
import { ThisReceiver } from '@angular/compiler';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  public UserName: string = '';
  public Pwd: string = '';
  public LoggedIn: boolean = false;
  public Auhtenticating: boolean = false;
  public usr: UserProfile = new UserProfile();

  constructor(private srv: DataSrvService, private rt : Router) {
  }

  public Authenticate() {
//    this.Auhtenticating = true;
    this.srv.auth(this.UserName, this.Pwd).subscribe(resp => {
      this.usr = resp as UserProfile;
//      this.Auhtenticating = false;
      if(this.usr.isAuth){
        this.LoggedIn = true;
        this.srv.userProfile = this.usr;
        this.srv.SetLoggedIn(this.usr);
        this.rt.navigate(['/']);
      }
    });
  }
}
