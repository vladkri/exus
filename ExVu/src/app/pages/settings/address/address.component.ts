import { Component, OnInit } from '@angular/core';
import { DataSrvService } from 'src/app/data-srv.service';
import { Address, AddressHeader } from 'src/app/models';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})

export class AddressComponent implements OnInit {

  public code: string;
  public addresses : AddressHeader = new AddressHeader;
  
  constructor(private srv:DataSrvService){
     this.code = '';
   }

   public ngOnInit(): void {
     this.code = this.srv.userProfile.code;
     this.srv.getAddresses().subscribe( resp => {
        this.addresses = resp;
     }
      );
   }
   public AddAddress() {
    let a = new Address();
    a.isNew = true
      this.addresses.addressList.push(a);
    }

    public SaveAddress(){
      this.addresses.status = 'Ogogo!';
      this.srv.saveAddress(this.addresses).subscribe(
        resp => {
          
        }
      );
    }
}
