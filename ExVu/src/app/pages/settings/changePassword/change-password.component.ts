import { Component, OnInit } from '@angular/core';
import { DataSrvService } from 'src/app/data-srv.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  public newPassword : string = "";
  public passwordChanged: boolean = false;
  public IsLoggedIn: boolean = false;

  constructor(private srv: DataSrvService) {
  }
  
  ngOnInit(): void {
    this.IsLoggedIn = this.srv.IsLoggedIn;
  }

  public ChangePassword(){
    if(this.newPassword == ''){
      return;
    }
    this.srv.changePassword(this.newPassword).subscribe(resp => {
      this.passwordChanged = true;
    });
  }
}
