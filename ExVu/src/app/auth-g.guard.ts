import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, map } from 'rxjs';
import { DataSrvService } from './data-srv.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGGuard implements CanActivate {
  
  constructor(private authService: DataSrvService, private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.isSignedIn();
  }

  isSignedIn(): Observable<boolean> {
    return this.authService.IsSignedIn().pipe(
        map((isSignedIn) => {
            if (!isSignedIn) {
                this.router.navigate(['login']);
                return false;
            }
            return true;
        }));
}  
}
