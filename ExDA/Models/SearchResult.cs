﻿#nullable disable

namespace ExDA.Models
{
    public class SearchResult
    {
        public int TotalITems { get; set; }
		public List<SearchItem> Results { get; set; } = new List<SearchItem>();
    }

	public class SearchItem
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Code { get; set; }
		public string CompanyName { get; set; }
		public string EntityType { get; set; }
	}
}
