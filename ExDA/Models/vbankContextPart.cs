﻿#nullable disable
using Microsoft.EntityFrameworkCore;

namespace ExDA.Models
{
    public partial class vbankContext : DbContext
    {
        private readonly string _con;
        public vbankContext(string con)
        {
            _con = con;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_con);
            }
        }
    }
}
