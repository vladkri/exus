﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ExDA.Models
{
	public partial class AddressPage
	{
		[NotMapped]
		public int PageSize { get; set; }

		[NotMapped]
		public int PageNum { get; set; }

		[NotMapped]
		public int CurrPage { get; set; }

		[NotMapped]
		public List<Address> Items { get; set; } = new List<Address>();

	}
}
