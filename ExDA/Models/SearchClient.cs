﻿#nullable disable

namespace ExDA.Models
{
    public class SearchClient
    {
        public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Account { get; set; }
		public string CompanyName { get; set; }
		public bool AllEmpty()
		{
			return string.IsNullOrWhiteSpace(FirstName) &&
				   string.IsNullOrWhiteSpace(LastName) &&
				   string.IsNullOrEmpty(CompanyName);
		}
	}
}
