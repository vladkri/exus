﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ExDA.Models
{
	public partial class Client
	{
		[NotMapped]
		public bool IsNew { get; set; }

		[NotMapped]
		public string LoginName { get; set; }

		[NotMapped]
		public string Password { get; set; }
	}
}
