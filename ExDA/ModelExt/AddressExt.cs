﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExDA.Models
{
	public partial class Address
	{
		[NotMapped]
		public bool IsNew { get; set; }
		[NotMapped]
		public string ClientCode { get; set; }
		[NotMapped]
		public string AddressType { get; set; }
	}
}
