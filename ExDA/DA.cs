﻿using ExDA.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
#nullable disable

namespace ExDA
{
    public class DA : IDA
    {
        private readonly string _con;
        public DA(string con)
        {
            _con = con;
        }

        public List<Client> FindClient(SearchClient sc)
        {
            using (var _ctx = new vbankContext(_con))
            {
                var res = _ctx.Client.Where(
                     c => c.FirstName.Contains(sc.FirstName) ||
                     c.LastName.Contains(sc.LastName) ||
                     c.CompanyName.Contains(sc.CompanyName) ||
                     c.ClientAccount.Where(ca => ca.Account.IBAN.Contains(sc.Account)).Any()).ToList();
                return res;
            }
        }

        public Client GetClient(string clientCode)
        {
            using (var _ctx = new vbankContext(_con))
            {
                var res = _ctx.Client.Where(c => c.Code == clientCode).
                       Include(c => c.ClientLogin).
                       Include(c => c.ClientAddress).
                       ThenInclude(c => c.Address).FirstOrDefault();
                return res;
            }
        }

        public Client ClientLogin(string name, string pwd)
        {
            using (var _ctx = new vbankContext(_con))
            {
                var log = _ctx.ClientLogin.Where(l => l.LoginName == name).FirstOrDefault();
                if (log == null)
                {
                    return null;
                }
                byte[] salt = Convert.FromBase64String(log.Salt);
                byte[] pws = Encoding.ASCII.GetBytes(pwd);
                byte[] hashBytes = Rfc2898DeriveBytes.Pbkdf2(pws, salt, 1, HashAlgorithmName.SHA512, 30);
                string hash = Convert.ToBase64String(hashBytes);
                if (hash == log.Password)
                {
                    var res = _ctx.Client.Where(c => c.Id == log.ClientId).FirstOrDefault();
                    return res;
                }
                return null;
            }
        }

        private string NewSalt() 
        {
			byte[] salt = new byte[4];
			Random.Shared.NextBytes(salt);
			return Convert.ToBase64String(salt);
		}

		public void SaveClient(Client cl)
        {
            string pwdTemp = "";
            using (var _ctx = new vbankContext(_con))
            {
                if (cl.IsNew)
                {
                    string code = "";
                    if (cl.EntityType == DBConst.CL_TYPE_BUSINESS)
                    {
                        code = cl.CompanyName;
                    }
                    else
                    {
                        code = cl.FirstName + cl.LastName;
                    }
                    code = code.ToLower();
                    if (code.Length > 20)
                    {
                        code = code.Substring(0, 20);
                    }
                    while (_ctx.Client.Any(c => c.Code == code))
                    {
                        if (code.Length < 20)
                        {
                            code += Random.Shared.Next(9).ToString();
                        }
                        else
                        {
                            string p1 = code.Substring(0, 9);
                            string p2 = code.Substring(10, 10);
                            p1 += Random.Shared.Next(9).ToString();
                            code = p1 + p2;
                        }
                    }
                    cl.Code = code;
                    var log = cl.ClientLogin.First();
                    byte[] salt = new byte[4];
                    Random.Shared.NextBytes(salt);
                    log.Salt = Convert.ToBase64String(salt);
					byte[] pws = Encoding.ASCII.GetBytes(log.Password);
                    pwdTemp = log.Password;
                    byte[] hashBytes = Rfc2898DeriveBytes.Pbkdf2(pws, salt, 1, HashAlgorithmName.SHA512, 30);
                    log.Status = "PRS";
                    log.Password = Convert.ToBase64String(hashBytes);
                    _ctx.Client.Add(cl);
                }
                else
                {
                    var existing = _ctx.Client.FirstOrDefault(c => c.Code == cl.Code);
                    if (existing == null)
                    {
                        throw new Exception("Internal error. client not found: " + cl.Code);
                    }
                    existing.FirstName = cl.FirstName;
                    existing.LastName = cl.LastName;
                    existing.CompanyName = cl.CompanyName;
                    if(! existing.ClientLogin.Any() && cl.ClientLogin.Any()) 
                    {
						existing.ClientLogin.Add(cl.ClientLogin.First());
						byte[] salt = new byte[4];
						Random.Shared.NextBytes(salt);
                        existing.ClientLogin.First().Salt = Convert.ToBase64String(salt);
                        existing.ClientLogin.First().Status = "PRS";
					}
				}
                _ctx.SaveChanges();
                if (cl.IsNew)
                {
                    cl.ClientLogin.First().Password = pwdTemp;
                }
            }
        }

        public bool ResetClientPassword(string loginName, string password)
        {

            using (var _ctx = new vbankContext(_con))
            {
                var log = _ctx.ClientLogin.Where(l => l.LoginName == loginName).FirstOrDefault();
                if (log == null)
                {
                    return false;
                }
                byte[] salt = Convert.FromBase64String(log.Salt);
                byte[] pws = Encoding.ASCII.GetBytes(password);
                byte[] hashBytes = Rfc2898DeriveBytes.Pbkdf2(pws, salt, 1, HashAlgorithmName.SHA512, 30);
                string hash = Convert.ToBase64String(hashBytes);
                log.Password = hash;
                _ctx.SaveChanges();
            }
            return true;
        }

        public bool IsClientLoginNameExist(string loginName)
        {
            using (var _ctx = new vbankContext(_con))
            {
                var log = _ctx.ClientLogin.Where(l => l.LoginName == loginName).FirstOrDefault();
                return log != null;
            }
        }

		public AddressPage GetAddressPaged(int pageSize, int currPage, string filter)
		{
            AddressPage ret = new AddressPage() { CurrPage = currPage, PageSize = pageSize };
            using (var _ctx = new vbankContext(_con))
            {
                var address = _ctx.Address.Skip((currPage - 1) * pageSize).Take(pageSize);
                ret.Items.AddRange(address);
            }
			return ret;
		}

		public List<Address> GetClientAddresses(string clientCode)
        {
			using (var _ctx = new vbankContext(_con))
			{
				var client = _ctx.Client.Where(c => c.Code == clientCode).Include(c => c.ClientAddress).ThenInclude(ca => ca.Address).First();
                var addresess = client.ClientAddress.Select(a => a.Address).ToList();
				return addresess;
			}

		}
		public bool SaveClientAddress(Address address)
		{
			using (var _ctx = new vbankContext(_con))
			{
				var client = _ctx.Client.Where(c => c.Code == address.ClientCode).Include(c => c.ClientAddress).First();
                ClientAddress ca = new ClientAddress();
                ca.Client = client;
                ca.Address = address;
                ca.AddressType = address.AddressType;
                ca.Status = "N";
				client.ClientAddress.Add(ca);
                _ctx.SaveChanges();
				return true;
			}

		}
	}
}
