﻿using ExDA.Models;

namespace ExDA
{
    public interface IDA
    {
        void SaveClient(Client cl);
		List<Client> FindClient(SearchClient sc);
		Client GetClient(string clientCode);

		Client ClientLogin(string name, string pwd);

		bool ResetClientPassword(string clientCode, string password);
		bool IsClientLoginNameExist(string loginName);
		AddressPage GetAddressPaged(int pageSize, int currPage, string filter);
		List<Address> GetClientAddresses(string clientCode);
		bool SaveClientAddress(Address address);
	}
}


