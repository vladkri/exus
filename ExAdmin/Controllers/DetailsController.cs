﻿using ExDA;
using ExDA.Models;
using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace ExAdmin.Controllers
{
	public class DetailsController : Controller
	{
		private readonly ILog _log = LogManager.GetLogger("Home");
		private readonly IConfiguration _cfg;
		private readonly IDA _da;
		public DetailsController(IConfiguration configuration, IDA da)
		{
			_cfg = configuration;
			_da = da;
		}

		public IActionResult Index()
		{
			return View();
		}

		public IActionResult NewAddress(string clientCode)
		{
			Address adr = new Address() {  ClientCode = clientCode, IsNew = true };
			return View("Address", adr);
		}

		public IActionResult SaveAddress(Address address)
		{
			var adr = _da.GetClientAddresses(address.ClientCode);
			if(adr.Any(a => a.AddressType == address.AddressType))
			{
				ModelState.AddModelError("Line", "Address of this type already exists");
				return View("Address", address);
			}
			_da.SaveClientAddress(address);
			return View("Address", address);
		}
	}
}
