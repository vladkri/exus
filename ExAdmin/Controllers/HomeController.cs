﻿using ExAdmin.Models;
using ExDA;
using ExDA.Models;
using log4net;
using log4net.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Configuration;
using System.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Security.Cryptography;

#nullable disable

namespace ExAdmin.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILog _log = LogManager.GetLogger("Home");
        private readonly IConfiguration _cfg;
        private readonly IDA _da;

        public HomeController(IConfiguration configuration, IDA da)
        {
            _cfg = configuration;
            _da = da;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SearchClient(SearchClient search)
        {
            if (search == null || search.AllEmpty())
            {
                return View(search);
            }
            var clients = _da.FindClient(search);
            SearchResult res = new SearchResult();
            res.TotalITems = clients.Count;
            clients.ForEach(c => res.Results.Add(new SearchItem() 
            { 
                FirstName = c.FirstName, LastName = c.LastName, Code = c.Code,
                EntityType = c.EntityType,  CompanyName = c.CompanyName
            })); 
			return View("SearchResult", res);
        }

		public IActionResult EditClient(string clientCode)
		{
			Client cl = _da.GetClient(clientCode);
			cl.IsNew = false;
            cl.LoginName = cl.ClientLogin.FirstOrDefault()?.LoginName;
			return View("Client", cl);
		}

		public IActionResult NewClient()
        {
            Client cl = new Client();
            cl.EntityType = "P";
            cl.IsNew = true;
            return View("Client", cl);
        }

        [HttpPost]
        public IActionResult SaveClient(Client cl)
        {
            if (cl.IsNew)
            {
                cl.CreatedBy = User.Identity?.Name;
                cl.UpdatedBy = User.Identity?.Name;
                cl.DateUpdated = DateTime.Now;
                cl.DateCreated = DateTime.Now;
                cl.LoginName = cl.FirstName.Subs(0, 2) + cl.LastName.Subs(0, 10);
                cl.ClientLogin.Add(new ClientLogin() { LoginName = cl.LoginName, Password = cl.LoginName});
            }
            _da.SaveClient(cl);
            cl.IsNew = false;
            cl.LoginName = cl.ClientLogin.FirstOrDefault()?.LoginName;
			cl.Password = cl.ClientLogin.FirstOrDefault()?.Password;
			return View("Client", cl);
        }

		public IActionResult ResetPwd(string clientCode)
		{
			Client cl = _da.GetClient(clientCode);
			cl.IsNew = false;
            if(!cl.ClientLogin.Any())  // somehow no login was eve created
            {
				cl.LoginName = cl.FirstName.Subs(0, 2) + cl.LastName.Subs(0, 10);
                int addon = 1;
                while (_da.IsClientLoginNameExist(cl.LoginName))
                {
                    cl.LoginName += addon++;
                }
                cl.ClientLogin.Add(new ClientLogin() { LoginName = cl.LoginName, Password = cl.LoginName });
                _da.SaveClient(cl);
			}
			cl.LoginName = cl.ClientLogin.FirstOrDefault()?.LoginName;
            cl.Password = cl.LoginName;
            _da.ResetClientPassword(cl.LoginName, cl.LoginName);
			return View("Client", cl);
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
