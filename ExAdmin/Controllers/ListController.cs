﻿using ExDA;
using ExDA.Models;
using log4net;
using Microsoft.AspNetCore.Mvc;

namespace ExAdmin.Controllers
{
	public class ListController : Controller
	{
		private readonly ILog _log = LogManager.GetLogger("List");
		private readonly IConfiguration _cfg;
		private readonly IDA _da;

		public ListController(IConfiguration configuration, IDA da)
		{
			_cfg = configuration;
			_da = da;
		}

		public IActionResult Index()
		{
			return View();
		}

		public IActionResult PrevAdrPage(AddressPage model)
		{
			model.CurrPage = model.CurrPage <= 0 ? 0 : (model.CurrPage+1);
			return View("AddressList", model);
		}
		public IActionResult NextAdrPage(int pageNumber)
		{
			var res = _da.GetAddressPaged(20, pageNumber, "");
            res.CurrPage = pageNumber;
			return View("AddressList", res);
		}

		public IActionResult AddressList()
		{
			var res = _da.GetAddressPaged(20, 1, "");
			return View("AddressList", res);
		}

		public IActionResult Branch()
		{
			return View();
		}
	}
}
