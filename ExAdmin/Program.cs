using ExDA.Models;
using Microsoft.AspNetCore.Authentication.Negotiate;
using Microsoft.EntityFrameworkCore;
using log4net.Config;
using log4net;
using ExDA;

namespace ExAdmin
{
    public static class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Program));
        public static void Main(string[] args)
        {
            XmlConfigurator.Configure(new FileInfo("log4net.config"));
            _log.Info("Starting");
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddSingleton(typeof(IDA), new DA(builder.Configuration.GetConnectionString("DefaultCon")));
            // Add services to the container.
            builder.Services.AddControllersWithViews();

            builder.Services.AddAuthentication(NegotiateDefaults.AuthenticationScheme)
               .AddNegotiate();

            builder.Services.AddAuthorization(options =>
            {
    // By default, all incoming requests will be authorized according to the default policy.
    options.FallbackPolicy = options.DefaultPolicy;
            });
            builder.Services.AddRazorPages();
            //builder.Services.AddDbContext<vbankContext>(options =>
            //    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultCon")));

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");

            app.Run();
        }
    }
}