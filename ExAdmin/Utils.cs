﻿using System.Runtime.CompilerServices;

namespace ExAdmin
{
	public class Utils
	{
	}

	public static class Ext
	{
		public static string Subs(this string str, int start, int len)
		{
			if(string.IsNullOrEmpty(str) || str.Length <= 2 || len <= 0 || start >= str.Length)
			{
				return str;
			}
			int l = Math.Min(str.Length, len);
			return str.Substring(start, l);
		}
	}
}
